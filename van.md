# Selina's first .md project
_PhD student_ 
_SED_ 
[website] (http://www.inria.fr)

## Dev 
- Python
- Matlab
- C / C++
- VHDL
- CST
- Git
    - github
    - gitlab
My favorite programming language is "python"

## My top 3 tv shows
1. Tyrant
1. The mentalist
1. The queen of the south

## My top 3 emoticons
:stuck_out_tongue: - :expressionless: - :kissing_smiling_eyes: 
code is:
```
:stuck_out_tongue: -  :expressionless: - :kissing_smiling_eyes:
``` 